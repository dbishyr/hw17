// hw17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
class Vector
{
public:
    Vector()
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << x << " " << y << " " << z << std::endl;
    }
    const double Mod()
    {
        return sqrt(x * x + y * y + z * z);
    }
private:
    double x, y, z;
};

class MyClass
{
public:
    void Show()
    {
        std::cout << a << " " << b << " " << s << std::endl;
    }
private:
    int a = 2;
    double b = 3.5;
    std::string s = "hello";
};

int main()
{
    Vector v(2, 3, 4);
    std::cout << v.Mod() << "\n";
    MyClass c;
    c.Show();
}

